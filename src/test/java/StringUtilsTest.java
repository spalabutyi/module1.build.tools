import org.example.StringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringUtilsTest {

    @Test
    @DisplayName(value = "Positive value Test")
    void isPositiveTest() {
        assertTrue(new StringUtils().isPositiveNumber("12"));
        assertTrue(new StringUtils().isPositiveNumber("79"));
        assertFalse(new StringUtils().isPositiveNumber("-5"));
        assertFalse(new StringUtils().isPositiveNumber("0"));
        assertFalse(new StringUtils().isPositiveNumber("abc"));
    }
}